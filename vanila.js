function formValidation() {
    try {
        console.log("Form Validation");
        var invalidMailMessage = document.getElementById("invalidMailMessage");
        var email = document.getElementById("email");
        if (email.value != "") {
            window.location.assign("/success-state.html")
        }
        else {
            displayError();
            return false;
        }
    } catch (e) {
        console.log("Error on formValidation: ",e.stack);
    }
}

function displayError() {
    invalidMailMessage.style.display = "block"
}